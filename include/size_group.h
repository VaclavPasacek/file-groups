#ifndef FILE_GROUPS_SIZE_GROUP_H_
#define FILE_GROUPS_SIZE_GROUP_H_

#include <memory>
#include <filesystem>

namespace _size_group_detail
{
	
class	SizeGroupPrivate;

} //namespace _size_group_detail

class SizeGroup
{
private:
	std::shared_ptr<_size_group_detail::SizeGroupPrivate> private_;

	SizeGroup(const SizeGroup&) = delete;
  SizeGroup(SizeGroup&&) = delete;
  SizeGroup& operator = (const SizeGroup&) = delete;
  SizeGroup& operator = (SizeGroup&&) = delete;
	
public:
		explicit SizeGroup();
		
		void Add(const std::filesystem::path& file);
		
    friend std::ostream& operator<<(std::ostream& os, const SizeGroup& size_group);
};

#endif // FILE_GROUPS_SIZE_GROUP_H_
