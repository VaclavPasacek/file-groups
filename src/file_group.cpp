#include "file_group.h"

#include <algorithm>
#include <fstream>
#include <iterator>
#include <iostream>


namespace // anonymous
{
  bool AreBinaryEqual(const std::filesystem::path& first_file_path,
                      const std::filesystem::path& second_file_path)
  {
    std::ifstream first_file(first_file_path, std::ifstream::in | std::ifstream::binary);
    std::ifstream second_file(second_file_path, std::ifstream::in | std::ifstream::binary);

    std::istreambuf_iterator<char> first_file_it(first_file);
    std::istreambuf_iterator<char> second_file_it(second_file);

    return std::equal(first_file_it, std::istreambuf_iterator<char>(),second_file_it);
  }
  
} // namespace anonymous

FileGroup::FileGroup(const std::filesystem::path& file)
  : first_file_(file)
  , files_()
  , file_group_mutex_()
{
  files_.push_back(first_file_);
}

bool FileGroup::TryAdd(const std::filesystem::path& file)
{
  if(AreBinaryEqual(first_file_, file))
  {
    std::lock_guard<std::mutex> lock(file_group_mutex_);
    files_.push_back(file);
    return true;
  }

  return false;
}

template<typename T>
std::ostream& ContainerToStream(std::ostream& os,
                                const T& container,
                                std::string delimiter = ", ")
{
  if (!container.empty())
  {
    auto it = container.begin();
    os << it->string();

    const auto end = container.end();

    for (++it; it != end; ++it)
    {
      os << delimiter << it->string();
    }
  }

  return os;
}

std::ostream& operator<<(std::ostream& os, const FileGroup& file_group)
{
  ContainerToStream(os, file_group.files_);

  return os;
}
