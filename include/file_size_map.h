#ifndef FILE_GROUPS_FILE_SIZE_MAP_H_
#define FILE_GROUPS_FILE_SIZE_MAP_H_

#include <memory>
#include <cstdint>

namespace _size_map_detail
{

class	FileSizeMapPrivate;

} //namespace _size_map_detail

class SizeGroup; 

class FileSizeMap
{
private:
  std::shared_ptr<_size_map_detail::FileSizeMapPrivate> private_;

  FileSizeMap(const FileSizeMap&) = delete;
  FileSizeMap(FileSizeMap&&) = delete;
  FileSizeMap& operator = (const FileSizeMap&) = delete;
  FileSizeMap& operator = (FileSizeMap&&) = delete;

public:
    explicit FileSizeMap();

    std::shared_ptr<SizeGroup> Acquire(uintmax_t file_size);
    
    friend std::ostream& operator<<(std::ostream& os, const FileSizeMap& file_size_map);
};

#endif // FILE_GROUPS_FILE_SIZE_MAP_H_
