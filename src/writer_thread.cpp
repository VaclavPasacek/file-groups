#include "writer_thread.h"

#include "file_size_map.h"

#include <iostream>
#include <vector>

WriterThread::WriterThread(FileSizeMap& file_size_map)
  : file_size_map_(file_size_map)
  , thread_(&WriterThread::Run, this)
{
}

void WriterThread::Run()
{
  Print();
}

void WriterThread::Print()
{
  std::cout << file_size_map_;
  std::cout << "---END---" << std::endl;
}

void WriterThread::Join()
{
  thread_.join();
}
