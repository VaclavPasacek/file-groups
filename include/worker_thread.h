#ifndef FILE_GROUPS_WORKER_THREAD_H_
#define FILE_GROUPS_WORKER_THREAD_H_

#include <thread>
#include <filesystem>

// forward declaration
class UnprocessedFiles;
class FileSizeMap;

class WorkerThread
{
private:
  UnprocessedFiles& unprocessed_files_;
  FileSizeMap&      file_size_map_;
  std::thread       thread_;

void ProcessFile(const std::filesystem::path& file);

public:
  explicit WorkerThread(UnprocessedFiles& unprocessed_files, FileSizeMap& file_size_map);

  void Run();
  void Join();
};

#endif // FILE_GROUPS_WORKER_THREAD_H_
