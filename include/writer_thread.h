#ifndef FILE_GROUPS_WRITER_THREAD_H_
#define FILE_GROUPS_WRITER_THREAD_H_

#include <thread>

// forward declaration
class FileSizeMap;

class WriterThread
{
private:
  FileSizeMap&      file_size_map_;
  std::thread       thread_;

void Print();

public:
  explicit WriterThread(FileSizeMap& file_size_map);

  void Run();
  void Join();
};

#endif // FILE_GROUPS_WRITER_THREAD_H_
