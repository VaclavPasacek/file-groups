#include "arg_parse.h"
#include "unprocessed_files.h"
#include "worker_thread.h"
#include "writer_thread.h"
#include "file_size_map.h"

#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>
#include <memory>

int main(int argc, char *argv[])
{
  int worker_thread_count=0;
  std::filesystem::path search_directory;
  try
  {
    std::tie(worker_thread_count, search_directory) = ParseCommandLineArgs(argc, argv);

    UnprocessedFiles unprocessed_files;
    FileSizeMap file_info_container;
    
    std::vector<std::shared_ptr<WorkerThread>> worker_threads;

    
    for(int i = 0; i < worker_thread_count; i++)
    {
      worker_threads.emplace_back(std::make_shared<WorkerThread>(unprocessed_files, file_info_container));
    }

    unprocessed_files.ProcessDirectory(search_directory);

    for(auto& worker_thread : worker_threads)
    {
      worker_thread->Join();
    }
    
    WriterThread writer_thread(file_info_container);
    writer_thread.Join();

  }
  catch(const std::exception& exc)
  {
      std::cerr << exc.what() << std::endl;
      return EXIT_FAILURE;
  }
  catch(...)
  {
      std::cerr << "Program failed due to unknown exception" << std::endl;
      return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
