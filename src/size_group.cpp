#include "size_group.h"

#include "file_group.h"

#include <thread>
#include <atomic>
#include <iostream>


namespace _size_group_detail
{

	struct Node
	{
	  std::shared_ptr<FileGroup>  file_group;
	  std::atomic<Node* > 				next;

	  explicit Node(const std::filesystem::path& file_path)
	    : file_group(std::make_shared<FileGroup>(file_path))
	    , next()
	  {
	  }
	};

  class NodeIterator
  {
  private:
    std::atomic<Node* >* current_;
    
    bool AddToExistingNode(const std::filesystem::path& file_path)
    {
      Node* current = current_->load(std::memory_order_relaxed);
      while(current != nullptr)
      {
        if(current->file_group->TryAdd(file_path))
        {
          return true;
        }
        
        current_ = &(current->next);
        current = current_->load(std::memory_order_relaxed);
      }
      
      return false;
    }
    
    void TryCreateNewNode(const std::filesystem::path& file_path)
    {
      Node* new_node = new Node(file_path);
      Node* possible_next_node = nullptr;
      if(!current_->compare_exchange_weak(possible_next_node, new_node,
                                    std::memory_order_release,
                                    std::memory_order_relaxed))
      {
        delete new_node;
        if(!AddToExistingNode(file_path))
        {
          TryCreateNewNode(file_path);
        }
      }
    }
    
  public:
    NodeIterator(std::atomic<Node* >* current)
      : current_(current)
    {
    }
    
    void Process(const std::filesystem::path& file_path)
    {
      if(!AddToExistingNode(file_path))
      {
        TryCreateNewNode(file_path);
      }
    }
  };
  
	class SizeGroupPrivate
	{
	protected:
	  std::atomic<Node* > first_;
    
	public:
	  explicit SizeGroupPrivate()
	    : first_(nullptr)
	  {
	  }
	  
    void Process(const std::filesystem::path& file_path)
    {
      NodeIterator iterator(&first_);
      iterator.Process(file_path);
    }
    
    friend std::ostream& operator<<(std::ostream& os, const SizeGroupPrivate& size_group)
    {
      Node* current = size_group.first_.load(std::memory_order_relaxed);
      while(current != nullptr)
      {
        os << *(current->file_group) << '\n';
        current = current->next.load(std::memory_order_relaxed);
      }
      
      return os;
    }
	};

} //namespace _size_group_detail


SizeGroup::SizeGroup()
	: private_(std::make_shared<_size_group_detail::SizeGroupPrivate>())
{
}

void SizeGroup::Add(const std::filesystem::path& file)
{
	private_->Process(file);
}

std::ostream& operator<<(std::ostream& os, const SizeGroup& size_group)
{
  os << *(size_group.private_);
  return os;
}
