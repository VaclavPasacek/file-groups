#include "worker_thread.h"

#include "unprocessed_files.h"
#include "file_size_map.h"
#include "size_group.h"

#include <iostream>
#include <vector>

WorkerThread::WorkerThread(UnprocessedFiles& unprocessed_files, FileSizeMap& file_size_map)
  : unprocessed_files_(unprocessed_files)
  , file_size_map_(file_size_map)
  , thread_(&WorkerThread::Run, this)
{
}

void WorkerThread::Run()
{
  bool processing_successful = true;
  do
  {
    std::optional<std::filesystem::path> file = unprocessed_files_.Retrieve();
    if(file)
    {
      ProcessFile(file.value());
    }

    processing_successful = file.has_value();
  }while(processing_successful);
}

void WorkerThread::ProcessFile(const std::filesystem::path& file)
{
  // std::cout << file << " : "<< std::this_thread::get_id() << " - " << file_size(file) << '\n';

  uintmax_t file_size = std::filesystem::file_size(file);
  std::shared_ptr<SizeGroup> size_group = file_size_map_.Acquire(file_size);
  size_group->Add(file);
}

void WorkerThread::Join()
{
  thread_.join();
}
