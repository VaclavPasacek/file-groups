#include "unprocessed_files.h"

#include <vector>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <iostream>

namespace _unprocessed_files
{
  class UnprocessedFilesPrivate
  {
  private:
    std::mutex mutex_;
    std::condition_variable condition_;
    std::vector<std::filesystem::path> files_;
    std::atomic<bool> done_;

    void AddFiles(const std::vector<std::filesystem::path>& new_files)
    {
      if(new_files.empty())
      {
        return;
      }

      // for(const auto& file : new_files)
      // {
      //   std::cout << "Going to add file " << file << '\n';
      // }

      {
        std::unique_lock lock(mutex_);
        files_.insert(files_.end(), new_files.begin(), new_files.end());
        lock.unlock();
      }

      condition_.notify_all();
    }

    void AddRecursiveDirectory(const std::filesystem::path& directory)
    {
      std::vector<std::filesystem::path> files;
      std::vector<std::filesystem::path> subdirectories;

      for(const auto& entry : std::filesystem::directory_iterator(directory))
      {
        if(std::filesystem::is_directory(entry))
        {
          subdirectories.push_back(entry);
        }
        else if(std::filesystem::is_regular_file(entry))
        {
          files.push_back(entry);
        }
      }

      AddFiles(files);
      for(const auto& directory : subdirectories)
      {
        AddRecursiveDirectory(directory);
      }
    }

  public:
    explicit UnprocessedFilesPrivate()
      : mutex_()
      , condition_()
      , files_()
      , done_(false)
    {
    }

    void ProcessDirectory(const std::filesystem::path& directory)
    {
      AddRecursiveDirectory(directory);
      done_ = true;
      condition_.notify_all();
    }

    std::optional<std::filesystem::path> Retrieve()
    {
        std::unique_lock lock(mutex_);
        do
        {
          if(!files_.empty())
          {
            std::filesystem::path file = files_.back();

            // std::cout << "Retrieve file " << file << '\n';

            files_.pop_back();
            return file;
          }

          if(!done_)
          {
            condition_.wait(lock);
          }
        }while(!done_);

      return std::nullopt;
    }
  };

} // namespace _unprocessed_files

UnprocessedFiles::UnprocessedFiles()
  : private_(std::make_shared<_unprocessed_files::UnprocessedFilesPrivate>())
{
}

void UnprocessedFiles::ProcessDirectory(const std::filesystem::path& directory)
{
  private_->ProcessDirectory(directory);
}

std::optional<std::filesystem::path> UnprocessedFiles::Retrieve()
{
  return private_->Retrieve();
}
