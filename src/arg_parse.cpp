# include "arg_parse.h"

#include <sstream>

// macro to simplify streaming of arguments into string
#ifndef _S
#define _S(__stream) ((std::ostringstream&)(std::ostringstream() << __stream )).str()
#endif

namespace {

const int MAX_THREAD_COUNT = 255;

int ParseWorkerThreadCount(std::string thread_count_text)
{
  int worker_thread_count = std::stoi(thread_count_text);
  if(worker_thread_count < 0 || worker_thread_count > MAX_THREAD_COUNT)
  {
    throw std::invalid_argument(
      _S("Error! Worker thread count must be in range from 0 to " <<
          MAX_THREAD_COUNT));
  }

  return worker_thread_count;
}

std::filesystem::path ParseSearchDirectory(std::string search_directory_text)
{
  std::filesystem::path path(search_directory_text);
  if(!std::filesystem::exists(path))
  {
    throw std::invalid_argument(_S("Directory " <<
                                    std::filesystem::absolute(path) <<
                                    " doesnt exist"));
  }

  if(!std::filesystem::is_directory(path))
  {
    throw std::invalid_argument(_S(std::filesystem::absolute(path) <<
                                    " isn't directory"));
  }

  return path;
}

} // anonymous namespace

std::tuple<int, std::filesystem::path> ParseCommandLineArgs(int argc, char *argv[])
{
  if(argc != 3)
  {
    throw std::invalid_argument(_S("Incorrect number of arguments " << argc <<
                             "\nIt should be started like" <<
                             "\nfile-groups <worker thread count> <file directory>"));
  }

  int worker_thread_count = ParseWorkerThreadCount(argv[1]);
  std::filesystem::path search_directory = ParseSearchDirectory(argv[2]);

  return std::make_tuple(worker_thread_count, search_directory);
}
