#ifndef FILE_GROUPS_UNPROCESSED_FILES_H_
#define FILE_GROUPS_UNPROCESSED_FILES_H_

#include <filesystem>
#include <optional>
#include <memory>

namespace _unprocessed_files
{
  // forward declaration
  class UnprocessedFilesPrivate;
} // namespace _unprocessed_files

class UnprocessedFiles
{
private:
  std::shared_ptr<_unprocessed_files::UnprocessedFilesPrivate> private_;

  UnprocessedFiles(const UnprocessedFiles&) = delete;
  UnprocessedFiles(UnprocessedFiles&&) = delete;
  UnprocessedFiles& operator = (const UnprocessedFiles&) = delete;
  UnprocessedFiles& operator = (UnprocessedFiles&&) = delete;

public:
  explicit UnprocessedFiles();
  // UnprocessedFiles(const& UnprocessedFiles) = delete;
  // void operator=(const& UnprocessedFiles) = delete;



  void ProcessDirectory(const std::filesystem::path& directory);
  std::optional<std::filesystem::path> Retrieve();
};

#endif // FILE_GROUPS_UNPROCESSED_FILES_H_
