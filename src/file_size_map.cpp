#include "file_size_map.h"

#include "size_group.h"

#include <map>
#include <shared_mutex>
#include <iostream>


namespace _size_map_detail
{

class FileSizeMapPrivate
{
private:
  std::map<uintmax_t, std::shared_ptr<SizeGroup> > size_map_;
  std::shared_mutex                                size_mutex_;
  
public:
  explicit FileSizeMapPrivate()
    : size_map_()
    , size_mutex_()
  {
  }
  
  std::shared_ptr<SizeGroup> Acquire(uintmax_t file_size)
  {
    // read lock
    {
      std::shared_lock<std::shared_mutex> read_lock(size_mutex_);
      // return existing size group
      auto size_group_it = size_map_.find(file_size);
      if(size_group_it != size_map_.end())
      {
        return size_group_it->second;
      }
    }
    
    // Write lock
    std::lock_guard<std::shared_mutex> write_lock(size_mutex_);
    
    // re-check size group existence
    auto size_group_it = size_map_.find(file_size);
    if(size_group_it != size_map_.end())
    {
      return size_group_it->second;
    }
    
    // return new size group
    auto new_size_group = std::make_shared<SizeGroup>();
    size_map_.emplace(file_size, new_size_group);
    return new_size_group;
  }
  
  friend std::ostream& operator<<(std::ostream& os, const FileSizeMapPrivate& file_size_map)
  {
    for(const auto& [size, size_group_ptr] : file_size_map.size_map_)
    {
      os << *size_group_ptr;
    }

    return os;
  }
};
    
} // namespace _size_map_detail

FileSizeMap::FileSizeMap()
  : private_(std::make_shared<_size_map_detail::FileSizeMapPrivate>())
{
}

std::shared_ptr<SizeGroup> FileSizeMap::Acquire(uintmax_t file_size)
{
  return private_->Acquire(file_size);
}

std::ostream& operator<<(std::ostream& os, const FileSizeMap& file_size_map)
{
  os << *(file_size_map.private_);

  return os;
}
