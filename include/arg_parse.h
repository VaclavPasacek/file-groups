#ifndef FILE_GROUPS_ARG_PARSE_H_
#define FILE_GROUPS_ARG_PARSE_H_

#include <tuple>
#include <filesystem>

std::tuple<int, std::filesystem::path> ParseCommandLineArgs(int argc, char *argv[]);

#endif // FILE_GROUPS_ARG_PARSE_H_
