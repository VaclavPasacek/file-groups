# File groups
Program has been developed and tested on Ubuntu 20.04.3 LTS with CMake 3.16.3 and GCC 9.4.0

## Instructions
Generate make
```
cmake -B <build directory>
```
Build
```
make --directory=<build directory>
```
Run
```
<build directory>/file-groups <thread count> <input directory>
```
