#ifndef FILE_GROUPS_FILE_GROUP_H_
#define FILE_GROUPS_FILE_GROUP_H_

#include <vector>
#include <filesystem>
#include <mutex>

class FileGroup
{
private:
  const std::filesystem::path         first_file_;
  std::vector<std::filesystem::path>  files_;
  std::mutex                          file_group_mutex_;

public:
  explicit FileGroup(const std::filesystem::path& file);

  bool TryAdd(const std::filesystem::path& path);
  void Print() const;

  friend std::ostream& operator<<(std::ostream& os, const FileGroup& file_group);
};

#endif // FILE_GROUPS_FILE_GROUP_H_
